# ta_html_css

This guide is intended for everyone who is soon going to be introduced to test automation, specifically test automation of websites with tools such as Selenium. With test automation tools you will hook into the software you are testing, in this case webpages. In order to do that it is incredibly useful if you have a working understanding of how websites are build up and the technologies used in doing so. 
This guide will take you along in making a simple website with these technologies, specifically HTML and CSS.

- HTML (Hypertext Markup Language) is what gives webpages their  structure.
- CSS (Cascading Style Sheets) is what gives webpages their styling. 

There are many other technologies involved in making a website like Javascript, JSON and many more. These all build upon HTML and CSS in order to do their thing and are not (as) important when you start out with automating tests for websites. 

This is also the reason that this guide is different from most guides out there that teach you to build websites. The end goal of this guide isn't to be able to create websites but to understand how websites and how you can use that knowledge when automating tests. 

**[Go to the guide.](html_css_guide.md)**
