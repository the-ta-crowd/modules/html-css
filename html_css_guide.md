- [1. Introduction](#1-introduction)
- [2. Before you start](#2-before-you-start)
- [3. HTML](#3-html)
  - [3.1. What are webpages?](#31-what-are-webpages)
  - [3.2. What is HTML?](#32-what-is-html)
    - [3.2.1. Tags & Elements](#321-tags--elements)
      - [3.2.1.1. Tags & Elements in relation to test automation](#3211-tags--elements-in-relation-to-test-automation)
    - [3.2.2. Attributes](#322-attributes)
      - [3.2.2.1. Attributes in relation to test automation](#3221-attributes-in-relation-to-test-automation)
  - [3.3. A note about the DOM and using browser developer tools to find elements](#33-a-note-about-the-dom-and-using-browser-developer-tools-to-find-elements)
  - [3.4. Pitfalls when trying to locate elements](#34-pitfalls-when-trying-to-locate-elements)
- [4. CSS](#4-css)
  - [4.1. What is CSS?](#41-what-is-css)
  - [4.2. CSS basics](#42-css-basics)
  - [4.3. Specific selectors](#43-specific-selectors)
  - [4.4. IDs](#44-ids)
  - [4.5. Classes](#45-classes)
  - [4.6. CSS in relation to test automation](#46-css-in-relation-to-test-automation)
- [5. A word about XPath](#5-a-word-about-xpath)
- [6. Attachment A: HTML element examples.](#6-attachment-a-html-element-examples)
  - [6.1. HTML form elements](#61-html-form-elements)
    - [6.1.1. Labels](#611-labels)
    - [6.1.2. Input elements](#612-input-elements)
    - [6.1.3. Select & option elements](#613-select--option-elements)
    - [6.1.4. Textarea elements](#614-textarea-elements)
    - [6.1.5. Other form related elements](#615-other-form-related-elements)
  - [6.2. Heading elements](#62-heading-elements)
  - [6.3. Lists](#63-lists)
  - [6.4. Iframes](#64-iframes)
  - [6.5. Tables](#65-tables)
  - [6.6. Closing words and additional reading](#66-closing-words-and-additional-reading)
- [7. Attachment B: In depth explanation of CSS selectors.](#7-attachment-b-in-depth-explanation-of-css-selectors)
  - [7.1. Attribute selectors](#71-attribute-selectors)
  - [7.2. Combining selectors](#72-combining-selectors)
  - [7.3. Pseudo classes](#73-pseudo-classes)
  - [7.4. Combinators (parent, child & sibling relations)](#74-combinators-parent-child--sibling-relations)
  - [7.5. Closing words and additional reading](#75-closing-words-and-additional-reading)

# 1. Introduction 

This guide is intended for everyone who is soon going to be introduced to test automation, specifically test automation of websites with tools such as Selenium. With test automation tools you will hook into the software you are testing, in this case webpages. In order to do that it is incredibly useful if you have a working understanding of how websites are build up and the technologies used in doing so. 
This guide will take you along in making a simple website with these technologies, specifically HTML and CSS.

- HTML (Hypertext Markup Language) is what gives webpages their structure.
- CSS (Cascading Style Sheets) is what gives webpages their styling. 

There are many other technologies involved in making a website like JavaScript, JSON and many more. These all build upon HTML and CSS in order to do their thing and are not (as) important when you start out with automating tests for websites. 

This is also the reason that this guide is different from most guides out there that teach you to build websites. The end goal of this guide isn't to be able to create websites but to understand how websites and how you can use that knowledge when automating tests. 

This also means that in some cases some things will not be explained in the fullest detail as it would overwhelm you or simply isn't relevant in the context of test automation. In many cases you will however find a link to the relevant [Mozilla MDN](https://developer.mozilla.org/en-US/docs/Web) page for more information. Mozilla MDN is a free to use, non-commercial knowledge platform for (web) developers and most often the most up-to-date and comprehensive resource for these technologies.

# 2. Before you start 

- You are going to create several example files when following this module. To keep them together it is recommended to create a directory on your computer where you can store them. 
- This module calls for a text editor. It is **not** recommended to use Windows notepad. Instead use an editor that supports so-called "syntax highlighting" as it will make it much easier for you to see what you are doing. For example, use one of the below editors: 
   - [Notepad++](https://notepad-plus-plus.org/downloads/)
   - [Visual Studio Code](https://code.visualstudio.com/download)

# 3. HTML

## 3.1. What are webpages? 

In the basis webpages is just simple text which the browser then takes and presents to you based on the content. 

As an example open a text editor and put in the following text:

```HTML
This is a web page
``` 

Save the file as `web_example.html` in the directory you made for this module. Now take the file you created and open it in your browser of choice. You will see that it displays a page with just the text you put in the file. 


## 3.2. What is HTML?
In the previous example the webpage was just simple text with no formatting or other elements. This is where HTML comes in, it is a so-called markup language that gives structure to a webpage and meaning to the content on it. HTML is an abbreviation for HyperText Markup Language.

### 3.2.1. Tags & Elements
Tags in HTML surround or accompany content and by doing so apply meaning to it. Tags are words surrounded by angle brackets `<` and `>`. Specific tags have specific meanings which is used by the browser to know how to process and present the content. 

```html
<tag>
```

There are basically two basic types of tags 

1. Tags surrounding content. They consist of an start and end tag where the end tag begins with a forward slash. The combination of the tags and content is what makes up an element. Different tags create different elements. 
    ```html
    <div>content</div>
    ```
2. There are also tags that are part of content. One example you are likely to encounter is the tag for line breaks.
    ```html
    <br>
    ```
    *Note*: You might also encounter single tags that look like a closing tag like in the example below. This has to do with the different versions of HTML. XHTML (an older version of HTML) did require single tags to be closed similar to paired tags. 
    ```html
    <br/>
    ```

A webpage is built with a particular structure of tags and elements as a foundation. This foundation basically provides the browser with the needed meta information to start rendering the page and if needed load extra information for styling and scripting. 

We are going to edit `web_example.html` to provide a minimal foundation for it to be considered a valid HTML page. 
In your editor change the file to contain the following text: 

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        This is a web page
    </body>
</html>
```

Save your document and refresh the web page in your browser. You will see that not much has been changed. This is because HTML gives structure to a page and doesn't provide styling as that is what CSS is for. 

What we did is provide basic structure and provide on extra bit of information to the browser you might have spotted. Let's go over the different pieces: 

- The first line `<!DOCTYPE html>` tells the browser what version of html you are providing it with. In this case it is HTML5 and for modern pages it is the most likely declaration you will encounter. Without this tag browsers will still recognize html tags but not know what flavor to use. As you can imagine this can lead to some rather odd behavior. 
- Next up we see that everything is wrapped `html` tags , this tells the browser that everything in between is in fact HTML. 
- We then encounter the `head` element, everything in between the `head` tags is so-called meta data. This means that it provides extra information to the browser and as result nothing in the `head` element will render. 
- The only meta information in `head` so far is the `title` element. You can see in your browser that this is used to display the title in the browser tab or browser title bar. Other meta information that can often found here is information about styling, scripting, language, character encoding and a few other things. 
- After the `head` element we encounter the `body` element. This is the content that will be rendered in the browser. For now it only contains our line of text. 

Next we will show how in the `body` HTML elements can change the meaning of the content. For the purpose of this demonstration we are going to add a list of items on the next line after our existing text. In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        This is a web page
        <br>
        <ul>
            <li> This is a list item </li>
            <li> This is also a list item </li>
        </ul>
    </body>
</html>
```

Now refresh the page in the browser and see the result. 
As you can see in the browser you now have our line of text, then a line break followed by two list items. 
 
We did this by providing structure to the text by providing the browser with the fact that it needs to render a line break through the `<br>` tag. Then we told the browser to create an unordered list through the `ul` tags. We then put list items in the list with the `li` tags. 

If we want to change the unordered list to an ordered list all we have to do is replace the `ul` tags with `ol` tags. Go ahead and try this. Adjust the code, save the file and reload the page in your browser

Instead of creating specific objects we can also use HTML elements to give further meaning to simple text. For example, we can put our text in a paragraph and put some emphasis on certain words. 
In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <p>
            This is a <strong>web<strong> page
        </p>
        <ul>
            <li> This is a list item </li>
            <li> This is also a list item </ul>
        </ul>
    </body>
</html>
```

Now refresh the page in the browser and see the result.  After refreshing the page you will see that **web** now is displayed as bold text. 

There are a lot of different tags and a lot of resulting elements that can be used in HTML. To name a few there are tags for hyperlinks, paragraphs, forms, video content, buttons, tables, etc. [You can visit the Mozilla developer docs](https://developer.mozilla.org/en-US/docs/Web/HTML/Element) for an extensive overview of tags and elements.

In attachment A you will find a variety of examples you can experiment with. 

#### 3.2.1.1. Tags & Elements in relation to test automation 
Effectively when looking at a webpage it is the elements you are targeting with test automation. Specifically you are most likely to target the elements in the page `body` you are looking for. 
It is because of this fact that it helps to be familiar with the basic tags and the elements they make up. Chances are that if you are trying to fill in a form with an automation tool you will have to find a form element containing input elements and button elements. 

### 3.2.2. Attributes 
Tags can also have *attributes* and provide extra information and context to tag and resulting elements. They consist of a key and a value. 
```html
<div attribute="value">content</div>
```

To give a tangible example, hyperlinks in pages need a specific attribute in order to link anywhere. Let's go ahead and explore this. In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <a href="https://www.capgemini.com">this is a hyperlink</a>
        <br>
        <a href="https://www.capgemini.com" target="_blank">this is also a hyperlink</a>
    </body>
</html>
```

Now refresh the page in the browser and see the result. As you can see we have added two slightly different hyperlinks to the page, separated by a line break tag. By using the `href` attribute we have told the browser that it needs to open the Capgemini website when these hyperlinks are clicked. 

Try clicking the first hyperlink, you will see that the link is opened in the same window/tab as your page. 
Now hit the back button in your browser and then click the second element, this one will open a new browser tab or window. This is the result of the `target` attribute, with it we provided the browser with additional information about how to handle the hyperlink when it was clicked. 


#### 3.2.2.1. Attributes in relation to test automation 

Attributes gives you more information to work with and to check against when automating your browser. If you want your test automation tool to look for a specific hyperlink then you need to find the hyperlink with the specific URL you are looking for. 

## 3.3. A note about the DOM and using browser developer tools to find elements
For the purpose of this module things have been somewhat simplified. Most webpages aren't single HTML files but rather created dynamically by the server (backend) but also in the browser itself through JavaScript. When this happens there isn't a base text version of the HTML but rather the Document Object Model (DOM) is manipulated. The DOM is effectively the representation of the HTML structure that the browser is showing to the user. 

When using a test automation tool you are targeting the DOM. This is important to know because browsers often offer two ways to look at the HTML of a page. When you right click and select "*view source*" you will see the bare text representation of the website before it is parsed by the browser. This isn't very helpful when trying to use a test automation tool. This is even more so the case for web pages where most HTML is dynamically created with JavaScript. 

If you right click and select "*inspect element*" most browsers will open the developer tools and the HTML shown there is the DOM representation of the page how it is seen by the browser. This is also what the test automation tool will be dealing with. 

![Right click inspect](img/right_click_inspect.png)

![Right click inspect](img/right_click_inspect_devtools.png)

What is important to know is that the DOM and the state of the webpage isn't static, if you see something visually changing on the webpage it also has an effect on the DOM. For example, hovering over an element can have an effect. 

## 3.4. Pitfalls when trying to locate elements

One thing to be aware of is that HTML elements are not always used in a way they are technically intended and sometimes a developer might create certain functionality from generic elements. What this effectively means is that you can use your knowledge of elements to recognize the page structure but that it isn't always a certainty that specific functionality is represented in a certain way. 

As an example, a form traditionally might look like this:

```html
<form action="/process_form.php">
  <input type="text" name="firstName">
  <input type="submit" value="Submit">
</form> 
```

This form has everything on board for the browser to handle it as a form:

- The is a `form` element with several input child elements. 
  - Here the `action` attribute on the `form` element tells the browser to send the contents of that form to `/process_form.php` when the form is submitted. 
- The content in this case is determined by the browser based on the `input` element where the `type` attribute has `text` as a value. 
- The form will be submitted when the user clicks on the input element with the value `submit` for the `type` attribute. 

But you might also encounter something like this:

```html
<div>
  <input type="text" name="firstName">
  <button type="button">Submit</button>
</div> 
```

Which doesn't use a form parent and uses a different type of button. On modern webpages you'll often encounter input elements that are handled like this as submission of the information is handled by JavaScript code and not the browser itself. 

Both of these examples will look exactly the same on the webpage itself and the difference is mostly in how they are handled by the browser. This is something to be aware of when you are trying to locate things you have seen in the browser but can't directly find when you are looking at the HTML representation. 


# 4. CSS 

## 4.1. What is CSS?
CSS is a language that is used to describe the style of an HTML document. In order to do so elements in an HTML document need to be classified with selectors in such a way that CSS can target it. 

## 4.2. CSS basics
In its core CSS is build up of two parts 

1. Selectors 
2. Properties inside those selector 

Whenever you see CSS the selector is the part before the curly bracket and its most simple form you can directly address html elements. To give you an example, to change something that affects all URLs it would look like this 

```css
a {
    color: red;
}
```


This is called a type selector, these simply select the different html elements, the example shown selects all `a` elements which are URLs. Used alone these selectors are rarely used in test automation as they are rarely unique. There are often used in combination with other selectors to make a selection more specific. For now we do stick with the simple example to show you how this works. We are going to add the CSS example in our HTML example, we do this by adding `style` tags to the `head` as this is meta information for the browser. There are also other ways to add CSS to a page, for example you can put all CSS in separate files and reference them with a `link` tag in the `head`. 

In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
        <style>
            a {
                color: red;
            }
        </style>
    </head>
    <body>
        <a href="https://www.capgemini.com">this is a hyperlink</a>
        <br>
        <a href="https://www.capgemini.com" target="_blank">this is also a hyperlink</a>
    </body>
</html>
```

Now refresh the page in the browser and see the result. As you can see both links have been colored red. 

## 4.3. Specific selectors

In our last example we targeted elements very broadly. So this is useful for generic things but not if you want to target specific elements. 

To target specific elements there are a whole host of options available but at its most basic you are looking at two kinds: 

1. IDs 
2. Classes

There also are a third and fourth kind of selector that can be a bit more complicated: 

- Attribute selectors
- Pseudo classes

This doesn't mean they aren't use, they are. However to go into them at this point in the guide will make things overly complex and make it less likely you get a good understanding of the basics. 
So in this chapter we will stick to IDs and Classes to familiarize you with how they work. Once you have completed the guide a further exploration of the more complex the selectors available can be found in [5. Attachment B: In depth explanation of CSS selectors.](#5-attachment-b-in-dept-explanation-of-css-selectors). 

## 4.4. IDs 
IDs are *supposed* to be unique to one element (this is **not** always the case so do not assume they are) and in CSS always start with #, in html you can also spot them rather easily as `id` attributes with the ID itself as the value:  

```html
<a id="thisThing">I am an url!</a>
```

To target the element with the `thisThing` id in CSS you would do that like so:

```css
#thisThing
```

As mentioned, sometimes IDs happen to not be unique. In that case you can make your selector more specific by combining the element type with the ID. So if `thisThing` is not unique but it is unique for all `a` elements you can do that like shown below. 

```css
a#thisThing
```
## 4.5. Classes

Classes are not unique to single elements but can be seen as a category of elements. They too are easy to spot in html

```html
<a class="theseThings">I am an url!</a>
```

And you target them with a dot like this 

```css
.theseThings
```

Or once again if you only need `a` elements with a specific class like `theseThings`. 

```css
a.theseThings
```

Let's explore this in an example. Our goal is to target all links, a specific group of links and finally one specific link. In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
        <style>
            a {
                color: red;
            }

            .grouped-link {
                border: solid 1px blue;
            }

            #specific-link {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <a href="https://www.capgemini.com" class="grouped-link">this is a hyperlink</a>
        <br>
        <a href="https://www.capgemini.com" class="grouped-link" id="specific-link">this is also a hyperlink</a>
        <br>
        <a href="https://www.capgemini.com">and this is also a hyperlink</a>
        <br>
        <span>This is only text</span>
    </body>
</html>
```

Now refresh the page in the browser and see the result. As you can see the first link is colored red as the CSS targets all links it also has the `grouped-link` class so it has a blue solid border of one pixel. The second link in addition to the `grouped-link` class also has the `specific-link` ID and as a result of that is shown in bold. The last link doesn't have any classes or IDs so only gets the generic styling for links. 

![Browser developer tools showing the example](img/css_dev_tools.png "Browser developer tools showing the example")

## 4.6. CSS in relation to test automation
The way selectors are used in CSS to apply styling to specific elements is the same way test automation tools and frameworks select those same elements to automate your tests. So by knowing how CSS works and is applied you can also make better selectors for your test automation tools and as a result of that better test the web applications you are testing. 

A neat little trick is that you can also use your CSS knowledge to not only make better selectors for your automation but also to debug your selectors in the browser. For this we are going to use the build in browser developer tools. 

# 5. A word about XPath

This guide has not covered XPath, it is however something you are likely to encounter when you start with test automation. It will however not be covered in this guide as it is not a technology used to create websites. Instead it is a technology to point to different parts of an XML document. Because HTML is effectively also XML XPath be used to select HTML element on a page. XPath is often used in places where CSS selectors can't be precise enough, CSS selectors aren't available, etc. So as mentioned earlier it is a technology you are likely to encounter when starting out with test automation. 

Using XPath isn't necessarily better or worse though there are some consideration to be made. For example, CSS selectors generally speaking are faster compared than XPath, XPath offers more ways to get to an element and a variety of elements more. Regardless of what technology is used, it is important that it is used consistently.

If you want to know more about XPath you can use these resources:
- [Mozilla MDN page about XPath](https://developer.mozilla.org/en-US/docs/Web/XPath)

# 6. Attachment A: HTML element examples.

Below you'll find some examples of HTML elements you might encounter with a short explanation of what they do. As with any other example in this guide you can 

## 6.1. HTML form elements

HTML forms are used to let users input data and have that data send back to a server to process them. A form is build up of a variety of elements for a variety of input methods all contained in one `form` element. Traditionally the `action` and `method` attributes in the `form` element determine what happens when a form is submitted, as `action` will contain the address where the data is send to and `method` determines if it is send as a `GET` or a `POST` request. In modern web development this is often no longer the case as often scripting in the browser is used to handle input, in fact often a `form` element will not be used and you'll simply encounter the variety of elements used for input on their own. Something that was also explained in paragraph 3.4.

In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <form action="/" method="post">
            <input type="text" id="inputExample" placeholder="this is placeholder text">
            <input type="submit" value="Submit">
        </form>
    </body>
</html>
```

After refreshing you will see that your page contains a single text `input` element showing the text we put in the `placeholder` attribute and a submit button. Go ahead and click on the submit button, you'll see that you navigate away from the webpage and are shown the root contents of the drive where you file is placed on. This is the result of the `action` attribute on the `form` element, since we put `/` there the form data is send to that location, which happens to be our local drive. This also means that in this instance nothing is done with the data you send. 

[Mozilla MDN about form elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form)

### 6.1.1. Labels 

Before we dive deeper into the variety of elements that allow for input we are going to introduce labels first. 
In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <form action="/" method="post">
            <label for="inputExample">Input example</label>
            <input type="text" id="inputExample" placeholder="this is placeholder text">
            <input type="submit" value="Submit">
        </form>
    </body>
</html>
```

After refreshing the page you will see that it now shows "Input example" before the `input` element. Go ahead and click on the text, you'll see that the `input` element gets focus. This is because `label` elements do what their name suggests, they represent an other item on the page and are linked through the `for` attribute which correspondents to the ID of the `input` element. 

[Mozilla MDN about label elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/label)

### 6.1.2. Input elements 

In the previous examples you might already have noticed that both the text input field and the submit button are `input` elements that derive their specific meaning from the `type` attribute. 

Let's explore a few more `input` variations we can encounter. In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <form action="/" method="post">
            <input type="text" id="inputExample" placeholder="this is placeholder text">
            <br><br>

            <label for="example1">Radio button option 1</label>
            <input type="radio" id="example1" name="radio_example" value="example1">
            <label for="example2">Radio button option 2</label>
            <input type="radio" id="example2" name="radio_example" value="example2">

            <br><br>

            <label for="colorExample">Color picker</label>
            <input type="color" id="colorExample" name="colorExample">

            <br><br>

            <label for="checkboxExample">Checkbox</label>
            <input type="checkbox" id="checkboxExample" name="checkboxExample">

            <br><br>

            <label for="numberExample">Numbers only input</label>
            <input type="number" id="numberExample" name="numberExample">

            <br><br>

            <label for="passwordExample">Password input</label>
            <input type="password" id="passwordExample" name="passwordExample">

            <br><br>

            <label for="fileExample">File picker</label>
            <input type="file" id="fileExample" name="fileExample">

            <br><br>

            <label for="dateExample">Date input</label>
            <input type="date" id="dateExample" name="dateExample">

            <br><br>

            <input type="submit" value="Submit">
        </form>
    </body>
</html>
```

After you refresh the page you will see a variety of input methods as the result of the using different `type` attributes. For a detailed explanation and even more options you can read more about these on [Mozilla MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input) as each of them also has a variety of specific attributes and characteristics. 

### 6.1.3. Select & option elements 

The `select` element represents a dropdown menu where `option` elements represent the selectable options in that menu. 

In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <form action="/" method="post">    
            <label for="example">Choose an option:</label>
            <select id="example" name="example">
                <option value="option1">option1</option>
                <option value="option2">option2</option>
                <option value="option3">option3</option>
            </select>
        </form>
    </body>
</html>
```

After refreshing the page you will be presented with a dropdown menu containing the three defined options. 

[Mozilla MDN about select & option elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/select)

### 6.1.4. Textarea elements 

So far we have seen `input` elements with the `type` attribute set to `text`, these only allow for input on one line. The `textarea` elements do provide a bigger input field allowing for text that spans over multiple lines. 

In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <form action="/" method="post">  
            <textarea rows="15" cols="45">This is a text area with pre-filled text.
            </textarea>
        </form>
    </body>
</html>
```

After refreshing the page you will see a text area with the pre-filled text. The `rows` attribute determines the amount of rows (lines of text) visible which impacts the height and the `cols` attribute determines the amount of characters (based on average-width) should fit on a single line which impacts the width. Note that the specific width and height can also be changed through CSS for styling purposes. 

[Mozilla MDN about textarea elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/textarea)

### 6.1.5. Other form related elements

There are a lot more elements related to forms for other purposes, as this attachment is mainly intended to give a few examples it will not detail all of them. For more details visit the [Mozilla MDN page about form elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form) and explore the related topics listed in the left menu. 

## 6.2. Heading elements

The different 6 types of heading elements represent the 6 levels of heading available in HTML. 

In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <h1>Level 1 heading element.</h1>
        <p>With a paragraph of text under it.<p>
        <h2>Level 2 heading element</h2>
        <p>With a paragraph of text under it.<p>
        <h3>Level 3 heading element</h3>
        <p>With a paragraph of text under it.<p>
        <h4>Level 4 heading element</h4>
        <p>With a paragraph of text under it.<p>
        <h5>Level 5 heading element</h5>
        <p>With a paragraph of text under it.<p>
        <h6>Level 6 heading element</h6>
        <p>With a paragraph of text under it.<p>
    </body>
</html>
```

After refreshing the page you will see the representation of each heading. 

[Mozilla MDN about heading elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements)

## 6.3. Lists

In HTML there are two list types you will encounter most often. Ordered lists defined with the `ol` element and unordered lists defined with the `ul` element. Items in lists are defined with the `li` elements. 

In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <ol>
            <li>ordered item1</li>
            <li>ordered item2</li>
            <li>ordered item3</li>
        </ol>
        <ul>
            <li>unordered item1</li>
            <li>unordered item2</li>
            <li>unordered item3</li>
        </ul>
        <ol>
            <li>ordered item1 with nested unordered list
                <ul>
                    <li>unordered item1</li>
                    <li>unordered item2</li>
                    <li>unordered item3</li>
                </ul>
            </li>
            <li>ordered item2</li>
            <li>ordered item3 with nested ordered list
                <ol>
                    <li>ordered item1</li>
                    <li>ordered item2</li>
                    <li>ordered item3</li>
                </ol>
            </li>
        </ol>
    </body>
</html>
```

After refreshing the page you will see the two types of list and also see how you can nest lists in a variety of manners. 

There is also a third type of list called a "description list" which is used to display key value pairs used to display terms with their description. These are not that widespread so this document doesn't describe them in detail. 

- [Mozilla MDN about ordered lists](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol)
- [Mozilla MDN about unordered lists](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul)
- [Mozilla MDN about description lists](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl)

## 6.4. Iframes

Iframes allow you to embed other webpages in the page. In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
    </head>
    <body>
        <iframe src="https://www.openstreetmap.org/export/embed.html?bbox=5.105161070823669%2C52.08750331551494%2C5.112242102622987%2C52.09037421838589&amp;layer=mapnik">
    </body>
</html>
```

After refreshing you will see a frame on the page where an openstreetmap.org map will be loaded. Do note that many webpages will simply refuse to be embedded out of security concerns, this is also why in the example openstreetmap.org is used as it specifically offers the option to embed maps. 

[Mozilla MDN about iframe elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe)

## 6.5. Tables 

In the late 90s early 2000s before CSS was properly capable of this HTML Table elements were often used to position things on page and in that sense used for styling. Their intended usage however is to represent tabular data, which in the most basic sense is data you might store in a spreadsheet. 

In your editor change `web_example.html` to contain the following:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>This is a web page title</title>
        <style>
            td, th {
                border: 1px solid #333;
            }
        </style>
    </head>
    <body>
        <h1>Complete table with headers and footers</h1>
        <table>
            <thead>
                <tr>
                    <th>Header 1</th>
                    <th>Header 2</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Body 1</td>
                    <td>Body 2</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>Footer 1</td>
                    <td>Footer 2</td>
                </tr>
            </tfoot>
        </table>
        <h1>Simple table</h1>
        <table>
            <tr>
                <td>content row 1</td>
                <td>content row 1</td>
                <td>content row 1</td>
            </tr>
            <tr>
                <td>content row 2</td>
                <td>content row 2</td>
                <td>content row 2</td>
            </tr>
        </table>
    </body>
</html>
```

After refreshing you will see two tables displayed under a descriptive heading. To make it easier to see we have used a tiny bit of CSS to give the cells in tables a border, no other styling as added. As you see tables can be made fairly complex or fairly simple. 

[Mozilla MDN about table elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table)

## 6.6. Closing words and additional reading

The previous examples are just a selection of all the HTML elements available for use and their options. They are just placed here as a few examples for you to experiment with a bit and to give you an idea of how HTML might be structured when you encounter it. 

On [Mozilla MDN](https://developer.mozilla.org/en-US/docs/Web/HTML) you will find a lot more information and reference material you can use in the future.

# 7. Attachment B: In depth explanation of CSS selectors. 

In chapter 3. we explored the basics of CSS through IDs and classes. In this attachment you'll find a further explanation of more detailed selectors. These will allow you to better target elements for your test automation. 



## 7.1. Attribute selectors

Sometimes IDs and Classes are not enough to specifically select an element. Which brings us to our third selector. 
Take the following HTML element

```html
<a href="http://www.google.com" target="_blank" title="this goes to google" >I am an url!</a>
```

As you can see it does not have any classes or IDs that will allow you to target it. It does however have a lot of other information available. Whenever you see something like this in an html element `classifier="value"` it is called an attribute. The shown element has three different attributes

- href
- target
- title

CSS allows you to select an element based on the selector. Say that you want to select all elements with the value `this goes to google` in their title attribute. We can do that like this

```css
[title="this goes to google"]
```

Or if you want all `a` elements that go to anything with `google` in the URL you can use 

```css
[href*="google"]
```

We can also select for just the attribute, so if we want any element with a `title` attribute we can use
```css
[title]
```

[See this page for all the options you can use](https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors)

## 7.2. Combining selectors 

You might already have spotted it, but you can combine different selectors to make them more specific and likely to target unique elements. For example, given the following html 

```html
<a href="http://www.google.com" target="_blank" title="this goes to google" id="google-url" class="outside-link" >I am an url!</a>
```

Combining all selectors we have used so far we could do something like 

```css
a#google-url.outside-link[href*="google"]
```

## 7.3. Pseudo classes
Pseudo classes are technically not really selectors but can still be very useful. Pseudo classes are added *after* a selector and indicate a state of the selector. For example, when hovering over an element on an HTML page you can target this with CSS with 

```css
a:hover
```

For test automation purposes there are a few pseudo classes that can help you in better targeting your element. So for example with `:not()` you can exclude some elements from a match. So say that you have the following html. 

```html
<div class="element">text</div>
<span class="element">text</span>
``` 

For example,: if you only want to select the div element, you can target this with the methods we used before or you can use

```css
.element:not(span)
```

There are also pseudo classes to determine if something is visible, checked (for checkboxes), the Xth child element and a whole lot more. 

[See this page for more information](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes)

## 7.4. Combinators (parent, child & sibling relations)

In many cases you can select the element you want by using the selectors for that specific element, often that isn't enough though. Consider the following html 

```html
<div id="parent1">
    <span class="child">
        This is the element we want.
    </span>
</div>
<div id="parent2">
    <span class="child">
        This is the element we do not want.
    </span>
</div>
```

With the selectors we previously covered it isn't possible to select just the first `span` element with the `.child` class. 
We can however use the parent as reference and use the following selector 

```css
#parent1 span.child
``` 

This effectively tells us that we want `span.child` only when it is the child element of `#parent1`. This is called a combinator and there are a variety of them that allow you to reference elements in a specific context. Before we continue a little bit about the terms used in here. 

**parent**
An element containing other elements. 

```html
<div class="parent">
    <span>hello world</span>
</div>
```

**child**
An element that is part of a parent element.

```html
<div class="parent">
    <span class="child">hello world</span>
</div>
```

**siblings**
Elements on the same level in HTML 
```html
<div class="parent">
    <span class="child1" id="sibling-of-child2">hello world</span>
    <span class="child2" id="sibling-of-child1">hello world</span>
</div>
```

The main methods to make use of this are listed below.

- `parentElement childElement` - descendant selector -  Any `childElement` that is the descended of `parentElement`
- `parentElement > childElement` - child selector- Any `childElement` that is the a direct child of `parentElement`. Meaning that it will only look one level down.
- `elementA + elementB` - adjacent sibling selector- selects `elementB` only if it immediately follows `elementA` in the same level. 
- `elementA ~ elementB` - general sibling selector- selects `elementB` if it is one of the next siblings following `elementA`.

## 7.5. Closing words and additional reading
While comprehensive the previous paragraphs were presented to give you a basic understanding of the wide variety CSS selectors available to you when you start with website test automation. For more in depth information and references Mozilla MDN is again the resource to use: 

- [Mozilla MDN about CSS selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)
- [Mozilla MDN about CSS in general](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)
